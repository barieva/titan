package ru.titan.calculator.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import ru.titan.calculator.domain.ParameterCalculationDto;
import ru.titan.calculator.domain.Result;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CalculatorServiceTest {
    @InjectMocks
    private CalculatorService calculatorService;
    @Mock
    private FunctionExecutorService functionExecutorService;

    @Before
    public void callPostConstructMethod() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        MockitoAnnotations.initMocks(this);
        Method postConstruct = CalculatorService.class.getDeclaredMethod("setTimeInterval", null);
        postConstruct.invoke(calculatorService);
    }

    @Test
    public void calculateOrderly() {
        String script = getScript();
        ParameterCalculationDto parameterCalculationDto = getParameterCalculationDto(script);
        when(functionExecutorService.getResult(1, script, 1)).thenReturn(new Result(1, "10.0", 10));
        when(functionExecutorService.getResult(1, script, 2)).thenReturn(new Result(1, "20.0", 10));
        Flux<String> orderedResults = calculatorService.calculateOrderly(parameterCalculationDto);
        StepVerifier.create(orderedResults).expectNext("1,10.0,10,0,<BR>20.0,10,0,<BR>").expectNextCount(1).expectComplete().verify();
    }

    @Test
    public void calculateOrderlyWithExceptionFromGetResult() {
        String script = getScript();
        ParameterCalculationDto parameterCalculationDto = getParameterCalculationDto(script);
        when(functionExecutorService.getResult(1, script, 1)).thenThrow(RuntimeException.class);
        when(functionExecutorService.getResult(1, script, 2)).thenThrow(RuntimeException.class);
        Flux<String> orderedResults = calculatorService.calculateOrderly(parameterCalculationDto);
        StepVerifier.create(orderedResults).verifyError(RuntimeException.class);
    }

    @Test
    public void calculateOutOfOrder() {
        String script = getScript();
        ParameterCalculationDto parameterCalculationDto = getParameterCalculationDto(script);
        when(functionExecutorService.getResult(1, script, 1)).thenReturn(new Result(1, "10.0", 10));
        when(functionExecutorService.getResult(1, script, 2)).thenReturn(new Result(1, "20.0", 10));
        Flux<String> orderedResults = calculatorService.calculateOutOfOrder(parameterCalculationDto);
        StepVerifier.create(orderedResults).expectTimeout(Duration.ZERO).verify();
    }

    @Test
    public void calculateOutOfOrderWithExceptionFromGetResult() {
        String script = getScript();
        ParameterCalculationDto parameterCalculationDto = getParameterCalculationDto(script);
        when(functionExecutorService.getResult(1, script, 1)).thenThrow(RuntimeException.class);
        when(functionExecutorService.getResult(1, script, 2)).thenThrow(RuntimeException.class);
        Flux<String> orderedResults = calculatorService.calculateOutOfOrder(parameterCalculationDto);
        StepVerifier.create(orderedResults).verifyError(RuntimeException.class);
    }

    private String getScript() {
        return "function calculate(index){ \n" +
                "return 10*index;}";
    }

    private ParameterCalculationDto getParameterCalculationDto(String script) {
        ParameterCalculationDto parameterCalculationDto = new ParameterCalculationDto();
        parameterCalculationDto.setIterationsNumber(1);
        parameterCalculationDto.setFirstFunctionText(script);
        parameterCalculationDto.setSecondFunctionText(script);
        return parameterCalculationDto;
    }
}