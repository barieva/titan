package ru.titan.calculator.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.titan.calculator.domain.Result;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class FunctionExecutorServiceTest {
    @Autowired
    private FunctionExecutorService functionExecutorService;

    @Test
    public void getResult() {
        String script = "function calculate(index)\n" +
                "{ return 10*index;}";
        Result result = functionExecutorService.getResult(1, script, 2);
        assertEquals("20.0", result.getFunctionResult());
        assertEquals(1, result.getIteration());
    }

    @Test
    public void getResultWithNegativeIterationAndIndex() {
        String script = "function calculate(index)\n" +
                "{ return 10*index;}";
        Result result = functionExecutorService.getResult(-11, script, -2);
        assertEquals("-20.0", result.getFunctionResult());
        assertEquals(-11, result.getIteration());
    }

    @Test
    public void getResultFromFunctionWithNoCalculateName() {
        String script = "function OTHER_NAME(index)\n" +
                "{ return 10*index;}";
        assertThrows(RuntimeException.class, () -> functionExecutorService.getResult(1, script, 1));
    }

    @Test
    public void getResultFromNullFunction() {
        assertThrows(RuntimeException.class, () -> functionExecutorService.getResult(1, null, 2));
    }

    @Test
    public void getResultFromFunctionWithError() {
        String script = "function calculate(index)\n" +
                "{ return ERROR;}";
        assertThrows(RuntimeException.class, () -> functionExecutorService.getResult(1, script, 1));
    }
}