package ru.titan.calculator.service;

import org.springframework.stereotype.Service;
import ru.titan.calculator.domain.Result;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import static java.lang.System.nanoTime;

@Service
public class FunctionExecutorService {
    private static final int MILLION = 1_000_000;

    public Result getResult(long iteration, String functionScript, int index) {
        long startTime = nanoTime();
        String functionResult = getFunctionResult(functionScript, index);
        long endTime = nanoTime();
        long calculationTimeInMilliseconds = (endTime - startTime) / MILLION;
        return new Result(iteration, functionResult, calculationTimeInMilliseconds);
    }

    private String getFunctionResult(String functionScript, int index) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        try {
            engine.eval(functionScript);
            Invocable invocable = (Invocable) engine;
            return String.valueOf(invocable.invokeFunction("calculate", index));
        } catch (ScriptException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}