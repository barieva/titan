package ru.titan.calculator.service;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.titan.calculator.domain.*;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.Duration.ofSeconds;
import static reactor.core.publisher.Flux.*;
import static ru.titan.calculator.domain.FunctionNumber.FIRST_FUNCTION;
import static ru.titan.calculator.domain.FunctionNumber.SECOND_FUNCTION;

@Service
public class CalculatorService {
    private Flux<Long> interval;
    @Value("${time.interval}")
    private int time;
    private FunctionExecutorService functionExecutorService;

    @Autowired
    public void setFunctionExecutorService(FunctionExecutorService functionExecutorService) {
        this.functionExecutorService = functionExecutorService;
    }

    @PostConstruct
    public void setTimeInterval() {
        interval = interval(ofSeconds(time));
    }

    public Flux<String> calculateOrderly(ParameterCalculationDto parameters) {
        AtomicInteger firstFunctionExecutedTimesNumber = new AtomicInteger();
        AtomicInteger secondFunctionExecutedTimesNumber = new AtomicInteger();
        AtomicInteger index = new AtomicInteger();
        Flux<OrderedResult> firstFunctionResultFlux = getOrderedResultFlux(parameters.getFirstFunctionText(), firstFunctionExecutedTimesNumber, index, parameters.getIterationsNumber());
        Flux<OrderedResult> secondFunctionResultFlux = getOrderedResultFlux(parameters.getSecondFunctionText(), secondFunctionExecutedTimesNumber, index, parameters.getIterationsNumber());
        interval.subscribe();
        return firstFunctionResultFlux.zipWith(secondFunctionResultFlux, (firstResult, secondResult) ->
                getOrderedResults(firstResult, secondResult)
        );
    }

    public Flux<String> calculateOutOfOrder(ParameterCalculationDto parameters) {
        AtomicInteger index = new AtomicInteger();
        Flux<String> firstFunctionResultFlux = getUnorderedResultFlux(FIRST_FUNCTION, parameters.getFirstFunctionText(), index, parameters.getIterationsNumber());
        Flux<String> secondFunctionResultFlux = getUnorderedResultFlux(SECOND_FUNCTION, parameters.getSecondFunctionText(), index, parameters.getIterationsNumber());
        return merge(firstFunctionResultFlux, secondFunctionResultFlux);
    }

    private Flux<OrderedResult> getOrderedResultFlux(String functionScript, AtomicInteger executedFunctions, AtomicInteger index, int iterationsNumber) {
        return Flux.from(interval).take(iterationsNumber)
                .map((iteration) -> getOrderedResult(functionScript, executedFunctions, index, iteration))
                .doOnNext(e -> executedFunctions.incrementAndGet());
    }

    private OrderedResult getOrderedResult(String functionScript, AtomicInteger executedFunctions, AtomicInteger index, Long iteration) {
        ++iteration;
        Result result = functionExecutorService.getResult(iteration, functionScript, index.incrementAndGet());
        return new OrderedResult(result, executedFunctions);
    }

    private String getOrderedResults(OrderedResult firstResult, OrderedResult secondResult) {
        long iteration = firstResult.getIteration();
        firstResult.getCalculatedFunctionsNumber().decrementAndGet();
        secondResult.getCalculatedFunctionsNumber().decrementAndGet();
        return new OrderedResultsHolder(iteration, firstResult, secondResult).toString();
    }

    private Flux<String> getUnorderedResultFlux(FunctionNumber functionNumber, String functionScript, AtomicInteger index, int iterationsNumber) {
        return from(interval).take(iterationsNumber).flatMap((iteration) -> getUnorderedResultMono(functionNumber, functionScript, index, iteration));
    }

    private Publisher<? extends String> getUnorderedResultMono(FunctionNumber functionNumber, String functionScript, AtomicInteger index, Long iteration) {
        ++iteration;
        Result result = functionExecutorService.getResult(iteration, functionScript, index.incrementAndGet());
        return Mono.just(new UnorderedResult(functionNumber, result).toString());
    }
}