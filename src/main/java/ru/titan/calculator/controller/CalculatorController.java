package ru.titan.calculator.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.titan.calculator.domain.ParameterCalculationDto;
import ru.titan.calculator.service.CalculatorService;

@RestController
public class CalculatorController {
    private CalculatorService calculatorService;

    @Autowired
    public void setCalculatorService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @PostMapping(value = "/calculate")
    public Flux<String> calculate(@RequestBody ParameterCalculationDto parameterCalculationDto) {
        if (parameterCalculationDto.isOrderedResponse()) {
            return calculatorService.calculateOrderly(parameterCalculationDto);
        } else {
            return calculatorService.calculateOutOfOrder(parameterCalculationDto);
        }
    }
}