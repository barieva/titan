package ru.titan.calculator.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderedResult extends Result {
    private AtomicInteger calculatedFunctionsNumber;

    public OrderedResult(Result result, AtomicInteger calculatedFunctionsNumber) {
        super(result.getIteration(), result.getFunctionResult(), result.getCalculationTime());
        this.calculatedFunctionsNumber = calculatedFunctionsNumber;
    }
}