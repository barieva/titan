package ru.titan.calculator.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

import static java.util.Arrays.asList;

@NoArgsConstructor
@Setter
@Getter
public class OrderedResultsHolder {
    private long iteration;
    private List<OrderedResult> orderedResults;

    public OrderedResultsHolder(long iteration, OrderedResult... orderedResults) {
        this.iteration = iteration;
        this.orderedResults = asList(orderedResults);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(iteration + ",");
        for (OrderedResult orderedResult : orderedResults) {
            stringBuilder.append(orderedResult.getFunctionResult() + ",")
                    .append(orderedResult.getCalculationTime() + ",")
                    .append(orderedResult.getCalculatedFunctionsNumber().get() + ",")
                    .append("<BR>");
        }
        return stringBuilder.toString();
    }
}