package ru.titan.calculator.domain;

import com.fasterxml.jackson.annotation.JsonGetter;

public enum FunctionNumber {
    FIRST_FUNCTION(1), SECOND_FUNCTION(2);
    private int number;

    FunctionNumber(int number) {
        this.number = number;
    }

    @JsonGetter
    public int getNumber() {
        return number;
    }
}