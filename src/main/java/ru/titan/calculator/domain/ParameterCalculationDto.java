package ru.titan.calculator.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParameterCalculationDto {
    private String firstFunctionText;
    private String secondFunctionText;
    private int iterationsNumber;
    private boolean orderedResponse;
}