package ru.titan.calculator.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UnorderedResult extends Result {
    private FunctionNumber functionNumber;

    public UnorderedResult(FunctionNumber functionNumber, Result result) {
        super(result.getIteration(), result.getFunctionResult(), result.getCalculationTime());
        this.functionNumber = functionNumber;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getIteration() + ",").append(getFunctionNumber().getNumber() + ",").append(getFunctionResult() + ",").append(getCalculationTime()).append("<BR>");
        return builder.toString();
    }
}